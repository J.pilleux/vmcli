use std::sync::RwLockWriteGuard;

use terminal_menu::TerminalMenuStruct;

use crate::vbox_commands::{
    clone_vm, remove_snapshot, remove_vm, restore_snapshot, start_vm, start_vm_headless, stop_vm,
    take_shapshot,
};

pub fn handle_menu_choice(mut vm_submenu: RwLockWriteGuard<'_, TerminalMenuStruct>, vm_name: &str) {
    let menu_choice = vm_submenu.selected_item_name();

    match menu_choice {
        "Start" => start_vm(vm_name),
        "Start headless" => start_vm_headless(vm_name),
        "Stop" => stop_vm(vm_name),
        "Snapshots" => {
            let snapshot_submenu = vm_submenu.get_submenu("Snapshots");
            handle_snapshots_submenu(snapshot_submenu, vm_name);
        }
        "Delete" => remove_vm(vm_name),
        "Clone" => {
            let new_vm_name = vm_submenu
                .get_submenu("Clone")
                .selection_value("New VM name")
                .to_owned();
            clone_vm(vm_name, &new_vm_name);
        }
        "x Exit" => {
            quit_program();
        },
        _ => eprintln!("Choice « {} » not handled yet", menu_choice),
    }
}

pub fn handle_snapshots_submenu(
    mut snapshot_submenu: RwLockWriteGuard<'_, TerminalMenuStruct>,
    vm_name: &str,
) {
    let choice = snapshot_submenu.selected_item_name();

    match choice {
        "Take" => {
            let snapshot_name = snapshot_submenu
                .get_submenu("Take")
                .selection_value("Snapshot name")
                .to_owned();
            take_shapshot(vm_name, &snapshot_name);
        }
        "Restore" => {
            let snapshot_name = snapshot_submenu
                .get_submenu("Restore")
                .selected_item_name()
                .to_owned();
            restore_snapshot(vm_name, &snapshot_name);
        }
        "Delete" => {
            let snapshot_name = snapshot_submenu
                .get_latest_menu_name()
                .expect("Cannot get snapshot name for delete")
                .to_owned();
            remove_snapshot(vm_name, &snapshot_name);
        }
        "x Exit" => {
            quit_program();
        },
        _ => eprintln!("Choice « {} » not handled yet", choice),
    }
}

pub fn quit_program() {
    println!("Ok bye!");
    std::process::exit(0);
}
