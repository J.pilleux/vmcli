use terminal_menu::{
    back_button, button, label, menu, mut_menu, run, string, submenu, TerminalMenuItem,
};

use crate::{
    menu_handlers::{handle_menu_choice, quit_program},
    vbox_commands::{get_vm_snapshot_names_list, get_vm_snapshot_tree}, vm_monitor::monitor_thread,
};

pub fn make_main_menu(machine_names: Vec<String>) {
    let vm_name_submenu = make_vm_names_menu(machine_names);

    let menu = menu(vec![
        label("Welcome in vmcli!"),
        label("Please select an action using the arrow keys"),
        label("To quit the application, use the q key"),
        label("--------------------------------------------"),
        label(""),

        submenu("VM list", vm_name_submenu),
        button("Check VM running"),

        button("x Exit")
    ]);

    run(&menu);

    let mut mm = mut_menu(&menu);
    let user_choice = mm.selected_item_name().to_owned();

    if mm.canceled() || user_choice == "x Exit" {
        quit_program();
    }

    if user_choice == "Check VM running" {
        monitor_thread();
    } else {
        let mut vm_list_submenu = mm.get_submenu("VM list");
        let vm_name = vm_list_submenu.selected_item_name().to_owned();

        if vm_name == "x Exit" {
            quit_program();
        }

        let vm_submenu = vm_list_submenu.get_submenu(&vm_name);
        handle_menu_choice(vm_submenu, &vm_name);
    }
}

fn make_vm_names_menu(content: Vec<String>) -> Vec<TerminalMenuItem> {
    let mut menu_vec: Vec<TerminalMenuItem> = Vec::new();
    // Adding content
    let mut content_vector: Vec<TerminalMenuItem> = content
        .iter()
        .map(|vm_name| {
            let snapshots_list = get_vm_snapshot_tree(vm_name);
            let vm_submenu = make_vm_actions_menu(vm_name, snapshots_list);
            submenu(vm_name, vm_submenu)
        })
        .collect();
    menu_vec.append(&mut content_vector);
    menu_vec.push(back_button("< Back"));
    menu_vec.push(button("x Exit"));

    menu_vec
}

fn make_vm_actions_menu(vm_name: &str, snapshots_list: String) -> Vec<TerminalMenuItem> {
    let snapshot_name_list = get_vm_snapshot_names_list(vm_name);
    let mut snapshot_names_submenu: Vec<TerminalMenuItem> =
        snapshot_name_list.iter().map(|snap| button(snap)).collect();
    snapshot_names_submenu.append(&mut vec![back_button("Back")]);

    let snapshot_name_remove_submenu: Vec<TerminalMenuItem> =
        make_snapshot_remove_submenu(snapshot_name_list, vm_name);

    let vm_menu = vec![
        label("------------------------------"),
        label(format!("VM « {} » snapshots list :", vm_name)),
        label(&snapshots_list),
        label("------------------------------"),
        label(""),

        label(format!("Choose an action for VM « {} »", vm_name)),
        label("------------------------------"),
        button("Start"),
        button("Start headless"),
        button("Stop"),
        submenu(
            "Remove",
            vec![
                label("-------------------------------------------"),
                label(format!(
                    "Are you sure you want to delete VM « {} » ?",
                    vm_name
                )),
                label("-------------------------------------------"),
                button("Remove"),
                back_button("< Back"),
            ],
        ),
        submenu(
            "Snapshots",
            vec![
                submenu(
                    "Take",
                    vec![
                        string("Snapshot name", "snap_name", false),
                        button("Take snapshot"),
                        back_button("< Back"),
                    ],
                ),
                submenu("Delete", snapshot_name_remove_submenu),
                submenu("Restore", snapshot_names_submenu),
                back_button("< Back"),
            ],
        ),
        submenu(
            "Clone",
            vec![
                string("New VM name", "new_name", false),
                button("Clone VM"),
                back_button("< Back"),
            ],
        ),
        back_button("< Back"),
        button("x Exit"),
    ];

    vm_menu
}

fn make_snapshot_remove_submenu(
    snapshots_list: Vec<String>,
    vm_name: &str,
) -> Vec<TerminalMenuItem> {
    let snapshot_name_remove_submenu: Vec<TerminalMenuItem> = if snapshots_list.len() != 0 {
        let mut snapshot_names: Vec<TerminalMenuItem> = snapshots_list
            .iter()
            .map(|snap| {
                submenu(
                    snap,
                    vec![
                        label("-------------------------------------------"),
                        label(format!(
                            "Are you sure you want to delete snapshot « {} » ?",
                            snap
                        )),
                        label("-------------------------------------------"),
                        button("Delete"),
                        back_button("Back"),
                    ],
                )
            })
            .collect();

        snapshot_names.push(back_button("< Back"));

        snapshot_names
    } else {
        vec![
            label(format!("No snapshots for VM {}", vm_name)),
            back_button("< Back"),
        ]
    };

    snapshot_name_remove_submenu
}
