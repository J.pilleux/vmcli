extern crate vmcli;

use vmcli::{vbox_commands::get_vm_names, vbox_menus::make_main_menu};

fn main() {
    loop {
        let machines = get_vm_names();
        make_main_menu(machines);
    }
}
