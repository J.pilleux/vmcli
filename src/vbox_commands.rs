use std::process::Command;

fn get_machine_name(line: &str) -> String {
    let split: Vec<&str> = line.split("\" ").collect();
    String::from(split[0].replace("\"", ""))
}

pub fn get_vm_names() -> Vec<String> {
    let output = Command::new("VBoxManage")
        .arg("list")
        .arg("--sorted")
        .arg("vms")
        .output()
        .expect("Command failed");

    let command_output = String::from_utf8_lossy(&output.stdout);
    let machines: Vec<String> = command_output
        .split("\n")
        .map(get_machine_name)
        .filter(|s| s != "")
        .collect();

    machines
}

pub fn get_vm_snapshot_tree(vm_name: &str) -> String {
    let output = Command::new("VBoxManage")
        .arg("snapshot")
        .arg(vm_name)
        .arg("list")
        .output()
        .expect("VM name does not exists");

    let command_output = String::from_utf8_lossy(&output.stdout);

    String::from(command_output)
}

pub fn get_vm_snapshot_names_list(vm_name: &str) -> Vec<String> {
    let output = Command::new("VBoxManage")
        .arg("snapshot")
        .arg(vm_name)
        .arg("list")
        .arg("--machinereadable")
        .output()
        .expect("VM name does not exists");

    let command_output = String::from_utf8_lossy(&output.stdout);
    let snapshot_list = command_output
        .split("\n")
        .filter(|snap| snap.starts_with("SnapshotName"));
    let snapshot_name_list = snapshot_list
        .map(|snap| {
            let split: Vec<&str> = snap.split("=").collect();
            String::from(split[1].replace("\"", "").trim())
        })
        .collect();

    snapshot_name_list
}

pub fn stop_vm(vm_name: &str) {
    Command::new("VBoxManage")
        .arg("controlvm")
        .arg(vm_name)
        .arg("poweroff")
        .status()
        .expect("VM name does not exists");
}

pub fn start_vm(vm_name: &str) {
    Command::new("VBoxManage")
        .arg("startvm")
        .arg(vm_name)
        .status()
        .expect("VM name does not exists");
}

pub fn start_vm_headless(vm_name: &str) {
    Command::new("VBoxManage")
        .arg("startvm")
        .arg(vm_name)
        .arg("--type")
        .arg("headless")
        .status()
        .expect("VM name does not exists");
}

pub fn clone_vm(vm_name: &str, new_vm_name: &str) {
    const VM_TMP_PATH: &str = "/tmp/__vm_clone_tmp.ova";

    Command::new("VBoxManage")
        .arg("export")
        .arg(vm_name)
        .arg("--output")
        .arg(VM_TMP_PATH)
        .status()
        .expect("VM name does not exists");

    Command::new("VBoxManage")
        .arg("import")
        .arg(VM_TMP_PATH)
        .arg("--vsys")
        .arg("0")
        .arg("--vmname")
        .arg(new_vm_name)
        .status()
        .expect("VM name does not exists");

    Command::new("VBoxManage")
        .arg("modifyvm")
        .arg(new_vm_name)
        .arg("--macaddress1")
        .arg("auto")
        .status()
        .expect("VM name does not exists");

    std::fs::remove_file(VM_TMP_PATH).expect("Cannot remove vm tmp path");
}

pub fn remove_vm(vm_name: &str) {
    Command::new("VBoxManage")
        .arg("unregistervm")
        .arg("--delete")
        .arg(vm_name)
        .status()
        .expect("VM name does not exists");
}

pub fn take_shapshot(vm_name: &str, snapshot_name: &str) {
    Command::new("VBoxManage")
        .arg("snapshot")
        .arg(vm_name)
        .arg("take")
        .arg(snapshot_name)
        .status()
        .expect("VM name does not exists");
}

pub fn restore_snapshot(vm_name: &str, snapshot_name: &str) {
    Command::new("VBoxManage")
        .arg("snapshot")
        .arg(vm_name)
        .arg("restore")
        .arg(snapshot_name)
        .status()
        .expect("VM name does not exists");
}

//VBoxManage snapshot "${_vm}" delete "${_snap}"
pub fn remove_snapshot(vm_name: &str, snapshot_name: &str) {
    Command::new("VBoxManage")
        .arg("snapshot")
        .arg(vm_name)
        .arg("delete")
        .arg(snapshot_name)
        .status()
        .expect("VM name does not exists");
}

pub fn get_list_running_vms() -> String {
    let output = Command::new("VBoxManage")
        .arg("list")
        .arg("runningvms")
        .output()
        .expect("Command failed");

    let command_output = String::from_utf8_lossy(&output.stdout);

    if command_output != "" {
        command_output.to_string()
    } else {
        "No vms are currently running".to_string()
    }
}
