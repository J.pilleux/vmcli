use std::{
    io::{stdin, stdout, Write},
    sync::mpsc::{self, TryRecvError},
    thread,
    time::Duration,
};

use termion::{event::Key, input::TermRead, raw::IntoRawMode};

use crate::vbox_commands::get_list_running_vms;

pub fn monitor_thread() {
    //setting up stdout and going into raw mode
    //printing welcoming message, clearing the screen and going to left top corner with the cursor
    //
    let (tx, rx) = mpsc::channel();
    let mut stdout = stdout().into_raw_mode().unwrap();

    let handle = thread::spawn(move || loop {
        let title_lines = vec![
            "Here is the list of currently running VMS :",
            "To quit this screen, press CTRL + q",
            "-------------------------------------------",
            "",
            ""
        ];
        let title = title_lines.join("\n\r");
        let running_message = get_list_running_vms();
        write!(
            stdout,
            "{}{}{}{}",
            termion::cursor::Goto(1, 1),
            termion::clear::All,
            &title,
            &running_message
        )
        .unwrap();
        stdout.flush().unwrap();
        match rx.try_recv() {
            Ok(_) | Err(TryRecvError::Disconnected) => {
                write!(
                    stdout,
                    r#"{}{}"#,
                    termion::cursor::Goto(1, 1),
                    termion::clear::All,
                )
                .unwrap();
                break;
            }
            Err(TryRecvError::Empty) => {}
        }
        thread::sleep(Duration::from_secs(1));
    });

    //detecting keydown events
    for c in stdin().keys() {
        //clearing the screen and going to top left corner
        match c.unwrap() {
            Key::Ctrl('q') => {
                let _ = tx.send(());
                handle.join().unwrap();
                break;
            }
            _ => (),
        }
    }
}
